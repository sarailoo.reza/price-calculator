<?php
namespace Helper;
use SplQueue;
class Core
{
    /**
     * @return mixed
     */
    public static function metabox_constants_form($post_id){

        global $post;
        $constant_fields = get_post_meta($post->ID, 'pc_constant_fields', true);
        wp_nonce_field( 'pc_constant_meta_box_nonce', 'pc_constant_meta_box_nonce' );
    ?>
        <table id="constants" class="repeatable-fieldset-one" width="100%">
        <thead>
            <tr>
                <th width="2%"></th>
                <th width="45%">Name</th>
                <th width="45%">Value</th>
                <th width="2%"></th>
            </tr>
        </thead>
        <tbody>
        <?php
    
        if ( $constant_fields ) :
            foreach ( $constant_fields as $field ) {
    ?>
        <tr>
            <td><a class="button remove-row" href="#">-</a></td>
            <td><input type="text" class="widefat" name="pc_constant_name[]" value="<?php if($field['pc_constant_name'] != '') echo esc_attr( $field['pc_constant_name'] ); ?>" /></td>
            <td><input type="text" class="widefat" name="pc_constant_value[]" value="<?php if ($field['pc_constant_value'] != '') echo esc_attr( $field['pc_constant_value'] ); ?>" /></td>
        </tr>
    <?php
            }
        else :
            // show a blank one
    ?>
        <tr>
            <td><a class="button remove-row" href="#">-</a></td>
            <td><input type="text" class="widefat" name="pc_constant_name[]" /></td>
            <td><input type="text" class="widefat" name="pc_constant_value[]" value="" /></td>
        </tr>
    <?php endif; ?>
    
        <!-- empty hidden one for jQuery -->
        <tr class="empty-row screen-reader-text">
            <td><a class="button remove-row" href="#">-</a></td>
            <td><input type="text" class="widefat" name="pc_constant_name[]" /></td>
            <td><input type="text" class="widefat" name="pc_constant_value[]" value="" /></td>   
        </tr>
        </tbody>
        </table>

        <p><a class="add-row button" href="#">Add another</a></p>  
    <?php
    }

    public static function metabox_questions_form($post_id){
        
        global $post;
        $question_fields = get_post_meta($post->ID, 'pc_question_fields', true);
        wp_nonce_field( 'pc_question_meta_box_nonce', 'pc_question_meta_box_nonce' );
    ?>
        <table id="questions" class="repeatable-fieldset-one" width="100%">
        <thead>
            <tr>
                <th width="2%"></th>
                <th width="90%">Title</th>
                <th width="2%"></th>
            </tr>
        </thead>
        <tbody>
        <?php
    
        if ( $question_fields ) :
            foreach ( $question_fields as $field ) {
    ?>
        <tr>
            <td><a class="button remove-row" href="#">-</a></td>
            <td><input type="text" class="widefat" name="pc_question_title[]" value="<?php if($field['pc_question_title'] != '') echo esc_attr( $field['pc_question_title'] ); ?>" /></td>
        </tr>
    <?php
            }
        else :
            // show a blank one
    ?>
        <tr>
            <td><a class="button remove-row" href="#">-</a></td>
            <td><input type="text" class="widefat" name="pc_question_title[]" /></td>
        </tr>
    <?php endif; ?>
    
        <!-- empty hidden one for jQuery -->
        <tr class="empty-row screen-reader-text">
            <td><a class="button remove-row" href="#">-</a></td>
            <td><input type="text" class="widefat" name="pc_question_title[]" /></td>    
        </tr>
        </tbody>
        </table>

        <p><a class="add-row button" href="#">Add another</a></p>  
    <?php
    }

    public static function metabox_calculations_form($post_id){

        global $post;
        $calculation_fields = get_post_meta($post->ID, 'pc_calculation_fields', true);
        wp_nonce_field( 'pc_calculation_meta_box_nonce', 'pc_calculation_meta_box_nonce' );
    ?>
        <table id="calculations" class="repeatable-fieldset-one" width="100%">
        <thead>
            <tr>
                <th width="2%"></th>
                <th width="45%">Name</th>
                <th width="45%">Formula</th>
                <th width="2%"></th>
            </tr>
        </thead>
        <tbody>
        <?php
    
        if ( $calculation_fields ) :
            foreach ( $calculation_fields as $field ) {
    ?>
        <tr>
            <td><a class="button remove-row" href="#">-</a></td>
            <td><input type="text" class="widefat" name="pc_calculation_name[]" value="<?php if($field['pc_calculation_name'] != '') echo esc_attr( $field['pc_calculation_name'] ); ?>" /></td>
            <td><input type="text" class="widefat" name="pc_calculation_formula[]" value="<?php if ($field['pc_calculation_formula'] != '') echo esc_attr( $field['pc_calculation_formula'] ); ?>" /></td>
        </tr>
    <?php
            }
        else :
            // show a blank one
    ?>
        <tr>
            <td><a class="button remove-row" href="#">-</a></td>
            <td><input type="text" class="widefat" name="pc_calculation_name[]" /></td>
            <td><input type="text" class="widefat" name="pc_calculation_formula[]" value="" /></td>
        </tr>
    <?php endif; ?>
    
        <!-- empty hidden one for jQuery -->
        <tr class="empty-row screen-reader-text">
            <td><a class="button remove-row" href="#">-</a></td>
            <td><input type="text" class="widefat" name="pc_calculation_name[]" /></td>
            <td><input type="text" class="widefat" name="pc_calculation_formula[]" value="" /></td>
        </tr>
        </tbody>
        </table>

        <p><a class="button add-row" href="#">Add another</a></p>  
    <?php
    }

    public static function metabox_finalresult_form($post_id){

        global $post;
        $finalresult_field = get_post_meta($post->ID, 'pc_finalresult_field', true);
        wp_nonce_field( 'pc_finalresult_meta_box_nonce', 'pc_finalresult_meta_box_nonce' );
    ?>
        <table id="finalresult" class="repeatable-fieldset-one" width="100%">
        <thead>
            <tr>
                <th width="2%"></th>
                <th width="90%">Formula</th>
                <th width="2%"></th>
            </tr>
        </thead>
        <tbody>
        <?php
    
        if ( $finalresult_field ) :
    ?>
        <tr>
            <td><a class="" href="#"></a></td>
            <td><input type="text" class="widefat" name="pc_finalresult_formula" value="<?php if($finalresult_field != '') echo esc_attr( $finalresult_field ); ?>" /></td>
        </tr>
    <?php
            
        else :
            // show a blank one
    ?>
        <tr>
            <td><a class="" href="#"></a></td>
            <td width="90%"><input type="text" class="widefat" name="pc_finalresult_formula" /></td>
        </tr>
    <?php endif; ?>
        </tbody>
        </table>
    <?php
    }


    public static function price_calculator_shortcode($atts = [], $content = null, $tag = ''){
        // normalize attribute keys, lowercase
        $atts = array_change_key_case((array)$atts, CASE_LOWER);
        
        // override default attributes with user attributes
        $pc_atts = shortcode_atts([
                                        'question_id' => 1,
                                    ], $atts, $tag);
        
        echo '<form action="" method="post">';                   
        $questions = get_post_meta( $pc_atts['question_id'], 'pc_question_fields',true );
        foreach ($questions as $meta_key => $v) {
            foreach ($v as $kk => $meta_value) {
                echo $meta_value."<br>";
                echo '<input type=text name="pc_user_answer[]">';
                break;
            }
        
        }
        echo '<input type="submit" name="pc_submit">';
        echo '</form>';
        // start output
        $o = '';
    
        // start box
        $o .= '<div class="wporg-box">';
    
        // title
        $o .= '<h2>' . esc_html__($pc_atts['question_id'], 'price-calculator') . '</h2>';
        // end box
        $o .= '</div>';
        
        
        if(isset($_POST['pc_submit'])){
            $c = array();
            $constants = get_post_meta( $pc_atts['question_id'], 'pc_constant_fields',true );
            $i=0;

            foreach ($constants as $meta_key => $const) {
                $f[$i] = sanitize_text_field($const["pc_constant_value"]);
                $i++;
            }
            
            $count = count($questions);
            $user_answer = $_POST['pc_user_answer'];
            for ( $i = 0; $i < $count; $i++ ) {
                if ( $user_answer[$i] != '' ) {
                    $q[$i] = sanitize_text_field( $user_answer[$i]);
                }
            }

            $calculations = get_post_meta( $pc_atts['question_id'], 'pc_calculation_fields',true );
            $i=0;
            foreach ($calculations as $meta_key => $calculation_formula) {
                Core::calculate($calculation_formula["pc_calculation_formula"],$f,$q,true,$i);
                $i++;
            }
            
            $final_result = get_post_meta( $pc_atts['question_id'], 'pc_finalresult_field',true );
            Core::calculate($final_result,$f,$q,false);
        }

        return $o;
        
    }
    private static function calculate($string,$f,$q,$is_calculation,$calculation_index=0){
        $str = $string;
        $string = str_split($string);
        for($i=0;$i<count($string);$i++){
            $var ="";
            $num ="";
            if($string[$i]=='{'){
                $var="";
                $num="";
                while($string[$i]!="}"){
                    if(is_numeric($string[$i])){
                        $num.=$string[$i];
                    }
                    else{
                        $var=$string[$i];
                    }
                    $i++;
                }
    
            if(!empty($var) and !empty($num)){
                global $c;
                if($var=="q" or $var=="Q" or $var=="f" or $var=="F" or $var=="c" or $var=="C"){
                    $helper_string="{".$var.$num."}";
                    if($var=="q" or $var=="Q"){
                        $str=str_replace($helper_string,(string)$q[(int)$num-1],$str);
                    }
                    else if($var=="c" or $var=="C"){
                        $str=str_replace($helper_string,(string)$c[(int)$num-1],$str);
                    }
                    else if($var=="f" or $var=="F"){
                        $str=str_replace($helper_string,(string)$f[(int)$num-1],$str);
                    }
                }
            }
            else{
                echo "error";
            }
            
            }
        }

        if($is_calculation){
            $c[$calculation_index] = eval('return '.$str.';');
        }
        else{
            echo "Final Result: ".eval('return '.$str.';');
        }

    }
}