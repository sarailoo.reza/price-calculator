<?php
namespace Actions;
use Helper\Core;

class Post
{
    public static function create_tables(){
    }
    
    public static function drop_tables(){
    }

    public static function hook_into_wordpress(){
        add_action('admin_enqueue_scripts',['Actions\Post', 'plugin_enqueue_scripts']);
        add_action( 'init', ['Actions\Post', 'create_posttype'] );
        add_action( 'add_meta_boxes', array( 'Actions\Post', 'create_metaboxes' ) );  
        add_action( 'save_post', array( 'Actions\Post', 'save_constants_metabox' ) );
        add_action( 'save_post', array( 'Actions\Post', 'save_questions_metabox' ) );
        add_action( 'save_post', array( 'Actions\Post', 'save_calculations_metabox' ) );
        add_action( 'save_post', array( 'Actions\Post', 'save_finalresult_metabox' ) );
        add_action( 'init', ['Actions\Post', 'create_shortcode'] );
        add_filter( 'manage_pc_question_posts_columns',['Actions\Post', 'set_custom_edit_pc_question_columns'] );
        add_action( 'manage_pc_question_posts_custom_column' ,['Actions\Post', 'shortcode_column'],10, 2);     
    }
    
    public static function plugin_enqueue_scripts() {   
        wp_enqueue_script( 'admin_fields_script', BASE_URL. 'assets/js/admin_fields.js', array('jquery'), '1.0' );
    }

    // remember to edit public & ... in posttype
    public static function create_posttype(){
        register_post_type('pc_question', array(
            'supports' => array('title'),
            'public' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'labels' => array(
                'name' => __( 'Question','price-calculator' ),
                'add_new_item' => __( 'Add New Questions ','price-calculator' ),
                'edit_item' => __( 'Edit Question','price-calculator' ),
                'all_items' =>  __( 'All Questions','price-calculator' ),
                'singular_name' => __( 'Question','price-calculator' ),
            ),
            'menu_icon' => 'dashicons-editor-help',
        ));       
    }


    public static function create_metaboxes(){
        add_meta_box(
			'pc_constants',
			__('Fixed','price-calculator'),
			array('Helper\Core' , 'metabox_constants_form' ),
			'pc_question'
        );
        add_meta_box(
			'pc_questions',
			__('Questions','price-calculator'),
			array('Helper\Core' , 'metabox_questions_form' ),
			'pc_question'
        );
        add_meta_box(
			'pc_calculations',
			__('Calculations','price-calculator'),
			array('Helper\Core' , 'metabox_calculations_form' ),
			'pc_question'
        );
        add_meta_box(
			'pc_finalresult',
			__('Final Result','price-calculator'),
			array('Helper\Core' , 'metabox_finalresult_form' ),
			'pc_question'
        );
    }

    public static function save_constants_metabox($post_id) {

        if ( ! isset( $_POST['pc_constant_meta_box_nonce'] ) ||
            ! wp_verify_nonce( $_POST['pc_constant_meta_box_nonce'], 'pc_constant_meta_box_nonce' ) )
            return;
    
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return;
    
        if (!current_user_can('edit_post', $post_id))
            return;
    
        $old = get_post_meta($post_id, 'pc_constant_fields', true);
        $new = array();
    
        $constant_names = $_POST['pc_constant_name'];
        $constant_values = $_POST['pc_constant_value'];
    
        $count = count( $constant_names );
    
        for ( $i = 0; $i < $count; $i++ ) {
            if ( $constant_names[$i] != '' ) :
                $new[$i]['pc_constant_name'] = stripslashes( strip_tags( $constant_names[$i] ) );
                $new[$i]['pc_constant_value'] = stripslashes( $constant_values[$i] );
            endif;
        }
    
        if ( !empty( $new ) && $new != $old )
            update_post_meta( $post_id, 'pc_constant_fields', $new );
        elseif ( empty($new) && $old )
            delete_post_meta( $post_id, 'pc_constant_fields', $old );
    }

    public static function save_questions_metabox($post_id){
        
        if ( ! isset( $_POST['pc_question_meta_box_nonce'] ) ||
            ! wp_verify_nonce( $_POST['pc_question_meta_box_nonce'], 'pc_question_meta_box_nonce' ) )
            return;
    
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return;
    
        if (!current_user_can('edit_post', $post_id))
            return;
    
        $old = get_post_meta($post_id, 'pc_question_fields', true);
        $new = array();
    
        $question_titles = $_POST['pc_question_title'];
    
        $count = count( $question_titles );
    
        for ( $i = 0; $i < $count; $i++ ) {
            if ( $question_titles[$i] != '' ) :
                $new[$i]['pc_question_title'] = stripslashes( strip_tags( $question_titles[$i] ) );
            endif;
        }
    
        if ( !empty( $new ) && $new != $old )
            update_post_meta( $post_id, 'pc_question_fields', $new );
        elseif ( empty($new) && $old )
            delete_post_meta( $post_id, 'pc_question_fields', $old );
    }

    public static function save_calculations_metabox($post_id){

        if ( ! isset( $_POST['pc_calculation_meta_box_nonce'] ) ||
            ! wp_verify_nonce( $_POST['pc_calculation_meta_box_nonce'], 'pc_calculation_meta_box_nonce' ) )
            return;
    
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return;
    
        if (!current_user_can('edit_post', $post_id))
            return;
    
        $old = get_post_meta($post_id, 'pc_calculation_fields', true);
        $new = array();
    
        $calculation_names = $_POST['pc_calculation_name'];
        $calculation_formulas = $_POST['pc_calculation_formula'];
    
        $count = count( $calculation_names );
    
        for ( $i = 0; $i < $count; $i++ ) {
            if ( $calculation_names[$i] != '' ) :
                $new[$i]['pc_calculation_name'] = stripslashes( strip_tags( $calculation_names[$i] ) );
                $new[$i]['pc_calculation_formula'] = stripslashes( $calculation_formulas[$i] );
            endif;
        }
    
        if ( !empty( $new ) && $new != $old )
            update_post_meta( $post_id, 'pc_calculation_fields', $new );
        elseif ( empty($new) && $old )
            delete_post_meta( $post_id, 'pc_calculation_fields', $old );
    }


    public static function save_finalresult_metabox($post_id){

        if ( ! isset( $_POST['pc_finalresult_meta_box_nonce'] ) ||
            ! wp_verify_nonce( $_POST['pc_finalresult_meta_box_nonce'], 'pc_finalresult_meta_box_nonce' ) )
            return;
    
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return;
    
        if (!current_user_can('edit_post', $post_id))
            return;

    
       $old = get_post_meta($post_id, 'pc_finalresult_field', true);

        $finalresult_formula = $_POST['pc_finalresult_formula'];

            if ( $finalresult_formula != '' ) :
                $new = stripslashes( $finalresult_formula );
            endif;
        if ( !empty( $new ) && $new != $old )
            update_post_meta( $post_id, 'pc_finalresult_field', $new );
        elseif ( empty($new) && $old )
            delete_post_meta( $post_id, 'pc_finalresult_field', $old );
    }

    public static function create_shortcode(){
        add_shortcode('price_calculator_section', array( 'Helper\Core', 'price_calculator_shortcode' ));
    }
    
    public static function set_custom_edit_pc_question_columns($columns) {
        unset( $columns['date'] );
        $columns['pc_question_shortcode'] = __( 'Shortcode', 'price-calculator' );
        $columns['date'] = __( 'Date', 'price-calculator' );
        return $columns;
    }
    public static function shortcode_column( $column, $post_id ){

        if($column == 'pc_question_shortcode'){
            _e( '[price_calculator_section question_id='.$post_id.']', 'price-calculator' );
        }
    }
}