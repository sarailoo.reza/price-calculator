jQuery(document).ready(function($) {
    
    $('.add-row').on('click', function() {
        var type = $(this).parent().siblings().closest('table').attr('id');
        var row = $('#'+type+' .empty-row.screen-reader-text').clone(true);
        row.removeClass('empty-row screen-reader-text');
        row.insertBefore('#'+type+' tbody>tr:last');
        return false;
    });

    $('.remove-row').on('click', function() {
        $(this).parents('tr').remove();
        return false;
    });

});